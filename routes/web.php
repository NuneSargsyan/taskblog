<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');

Route::middleware('auth')->get('/blog', 'BlogController@blog');
Route::get('single/blog/{id}', 'BlogController@single');
Route::get('/blog/add', 'BlogController@index');
Route::get('/blog/delete/{id}', 'BlogController@destroy');
Route::post('/blog/store', 'BlogController@store');

Route::post('/comment/store/{id}', 'CommentController@store');
Route::get('/comment/delete/{id}', 'CommentController@destroy');


Route::get('/user/{id}','UserController@index');
Route::post('/like/{id}','LikeController@like');
Route::post('/dislike/{id}','LikeController@dislike');


Route::get('/send/mail','SendMailController@index');
Route::get('/email','SendMailController@email');
Route::post('/send/mail','SendMailController@send');
