<?php

use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blogs')->insert([
            'title' => 'An Interesting History',
            'description' => 'One thing that no one questions is that Lerem Ipsum is based upon Latin. What’s more, that Latin dates back to 45 BCE in Cicero’s first book of De Finibus Bonorum et Malorum (“On the Extremes of Good and Evil”).

There are a few passages taken from the book. But the main one — that includes “lorem ipsum” — is “qui dolorem ipsum.” According to Google Translate, this means “their distressing anguish and the very.”',
            'user_id' => 1,
        ]);
    }
}
