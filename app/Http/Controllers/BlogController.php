<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Comment;
use App\Http\Requests\BlogRequest;
use App\Like;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
         $this->middleware('auth');
    }
    public function blog(){
        $blog = Blog::orderBy('id','desc')->paginate(5);
        return view('blog')->with('blogs',$blog);
    }
    public function single($id){
        $comments = Comment::with('user')->where('blog_id',$id)->get();
        $blogs = Blog::with('user')->where('id',$id)->first();
        $likes = count(Like::where('blog_id', $id)->get());
        $isLike = count(Like::where('blog_id', $id)->where('user_id', Auth::id())->get());
        return view('single', compact('likes', 'isLike'))->with('blogs',$blogs)->with('comments',$comments);

    }
    public function index()
    {
        $blog =Blog::all();
        return view('add_blog')->with('blogs',$blog);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlogRequest $request)
    {
        try {
            $blog = new Blog();
            $blog->user_id =Auth::id();
            $blog->title = $request->title;
            $blog->description = $request->description;
            $blog->save();


        }catch (Exception $err){
            return $err;
        }
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blog::where('id',$id)->delete();
        return  redirect()->back();
    }
}
