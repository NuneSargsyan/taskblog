<?php

namespace App\Http\Controllers;

use App\Mail\TestMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class SendMailController extends Controller
{
    function index(){
        return view('email.form');
    }
    function email(){
        $mail = Mail::all();
        return view('admin/email')->with('mails',$mail);
    }
    public function send(Request $request){

        $body = [
            'title' => $request->title,
            'description'=> $request->description
        ];
        Mail::to($request->email)->send(new TestMail($body));
        return redirect()->back();
    }
    public function store(Request $request)
    {
        $mail = new Mail();
        $mail->name = $request->name;
        $mail->email = $request->email;
        $mail->save();
        return  redirect()->back();
    }
    public function mail(){
        return view('email.addMail');
    }
}
