@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-5 pl-3 pr-3">

                        <form action="/blog/store" method="post">
                            @csrf
                            <div class="d-flex justify-content-center flex-column">
                                <input type="text" name="title" placeholder="Title" class="form-control mb-3 mt-3">
                                <textarea type="text" name="description" placeholder="Description" class="form-control  mb-3"></textarea>
                                <button class="btn btn-success mb-3">Add blog</button>
                            </div>
                        </form>

                </div>
            </div>
        </div>
    </div>
@endsection
