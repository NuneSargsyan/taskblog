@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-5">
                    <div class="card-header">
                        <h3>{{$users['name']}}</h3>
                        <p>{{$users['email']}}</p>
                    </div>
                    <div class="card-body">
                        @foreach($blogs as $key)
                            <div class="card mb-5">
                                <div class="card-header">
                                     <h3>{{$key['title']}}</h3>
                                </div>
                                <div class="card-body">
                                    <p>{{$key['description']}}</p>
                                    @if($key['user_id'] == Auth::id())
                                        <p><a href="/blog/delete/{{$key['id']}}">Delete</a></p>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="d-flex justify-content-center">
        {!!$blogs->links() !!}
    </div>
@endsection
