@extends('layouts.app')

@section('content')
    <div class="container">
        @foreach($blogs as $key)
            <a href="single/blog/{{$key['id']}}">
                <h3>{{$key['title']}}</h3>
                <p>{{$key['description']}}</p>
            </a>
        @endforeach
    </div>

    {!!$blogs->links() !!}
@endsection
