@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card mb-5">
                    <div class="card-header infos-blog">
                        <h3>{{$blogs['title']}}</h3>
                        <div class="info-blog">
                            <div class="user">
                                <div class="avatar-user">
                                    <img src="https://cdn.pixabay.com/photo/2016/08/08/09/17/avatar-1577909_960_720.png" alt="">
                                </div>
                                <h4><a href="/user/{{$blogs['user_id']}}">{{$blogs['user']['name']}}</a></h4>
                            </div>
                        </div>
                        <div class="description-body">

                            <p>{{$blogs['description']}}</p>
                        </div>
                    </div>
                    <div class="card-body">
                        <h4>Comments</h4>
                        @foreach($comments as $key)
                            <div class="comment-item">
                                <p>{{ $key['user']['name'] }}</p>
                                <p class="card-comment">{{$key['comment']}}</p>
                                @if($key['user']['id'] == Auth::id())
                                    <p>
                                        <a href="/comment/delete/{{$key['id']}}">Delete</a>
                                    </p>
                                @endif
                                <button class="like" data-like="{{ $isLike }}" data-post-id="{{ $blogs['id'] }}">like (<span class="like-count">{{ $likes }}</span>)</button>
                            </div>
                        @endforeach
                            <form action="/comment/store/{{$blogs['id']}}" method="post">
                                @csrf
                                <textarea name="comment" placeholder="Comment" class="form-control mb-3 mt-3"></textarea>
                                <button class="btn btn-success">Add comment</button>
                            </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
