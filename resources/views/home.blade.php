@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                @foreach($blogs as $key)
                    <div class="card mb-5">
                        <div class="card-header">
                            <a href="single/blog/{{$key['id']}}">
                                <h3>{{$key['title']}}</h3>
                            </a>
                        </div>
                        <div class="card-body">
                            @if(strlen($key['description'])>100)
                                {{ substr($key['description'], 0, 100) . '...' }}
                            @else
                                {{ $key['description'] }}
                            @endif
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container">

        </div>
    </div>
    <div class="d-flex justify-content-center">
        {!!$blogs->links() !!}
    </div>
@endsection
